Feature: Administrator's Adv. Accounts tab
  As an admin, I want to be able to see the advertiser account list, so I know how who is registering and what not

  Background:
    Given I am logged in as an admin user who has many accounts
    And I am on the Adv. Accounts tab in the Admin pages

  Scenario: Adv. Accounts tab should have expected UI elements
    When I look at the user interface for the tab
    Then I see the expected UI elements for the Advertiser Account tab

  Scenario: The Adv. Accounts table can be sorted by columns
    When I click the column headers on the Advertiser Account tab
    Then the columns sort and reverse sort, as expected
    And a chevron icon appears in the header of the sorted column