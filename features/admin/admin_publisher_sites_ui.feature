Feature: Publisher Sites UI for Admins

  As a product owner, I want the ability for admins (publisher or advertiser account) to be able
  to see a list of publisher sites in order to see their various stats and set site categories
  and site approval status.

  Background:
    Given several publisher sites exist with various categories and statuses

  Scenario: Admins can see the Publisher sites page
    Given I am logged in as an admin user
    When I go to the Publisher Home tab
    Then I can see the appropriate sites and information about them


