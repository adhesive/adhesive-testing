Feature: Administrator's Adv. Sites tab
  As an admin, I want to be able to see the advertiser site list, so I know
  who is registering what sites and what not

  Background:
    Given I am logged in as an admin user who has many accounts
    And I am on the Adv. Sites tab in the Admin pages

  Scenario: Adv. Sites UI elements all appear as expected

  Scenario: The Adv. Sites table columns are sortable

  Scenario: Clicking the Site name opens the Site's URL in a new tab/window

  Scenario: Sites can be given categories which will then display in the table

  Scenario: A Site's approval status can be updated on this tab