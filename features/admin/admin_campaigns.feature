Feature: Campaigns tab in the Admin pages
  As an admin, I want to be able to see the advertiser campaign list, so I
  know what the advertisers are running and whatnot

  Background:
    Given I am logged in as an admin user who has many accounts
    And I am on the Campaigns tab in the Admin pages

  Scenario: Campaigns tab UI has all expected elements

  Scenario: The Campaigns table is sortable by columns

  Scenario: Clicking the Site name opens the Site's URL in a new tab/window

  Scenario: A Campaign's approval status can be toggled on this page