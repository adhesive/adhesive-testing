Feature: Calculating the cost to an Advertiser
  As an advertiser, I want to know my costs based on the bid prices and clicks

  Background:
    Given I am logged in as an advertiser with several active sites and campaigns

  Scenario: Basic cost is the bid price multiplied by count of clicks
    Given a campaign with a bid price of $2.00
    When there are 50 clicks on ads in that campaign
    Then the Cost is $100

  Scenario: Multiple bid prices are reflected in the Advertiser's cost
    Given a campaign with a bid price of $1.00
    And a second campaign with a bid price of $0.50
    When there are 100 clicks on ads in the first campaign
    And there are 200 clicks on ads in the second campaign
    Then the Cost is $200

  Scenario: Updates to bid prices are reflected in the Advertiser's cost
    Given a campaign with a bid price of $0.75
    And 100 clicks on ads at that price
    When the campaign's bid price is changed to $1.00
    And there are 100 clicks on ads at the new bid price
    Then the Cost is $175

  Scenario: Average Cost per Click is Cost divided by the click count
    Given a campaign with a Cost of $200
    When the click count for the campaign is 100
    Then the Average CPC is $2