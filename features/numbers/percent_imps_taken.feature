Feature: The percentage of impressions taken
  As a Publisher, I want to know the ratio of impressions served versus calls requesting that they be served

  Background:
    Given I am logged in as a Publisher with multiple active Accounts/Sites/Campaigns

  Scenario: The % of imps taken is the number of impressions divided by the number of calls, times 100
    Given the campaign served 5000 impressions
    And there were 6000 calls to the campaign
    Then the % of imps taken is 83.33
