Feature: Publisher Click-Through Rate
  As a Publisher, I want to know what my Click-through Rate is for my campaigns.

  Background:
    Given I am logged in as a Publisher with multiple active Accounts/Sites/Campaigns

  Scenario: The click-through rate is the click count divided by the impressions count, times 100
    Given a campaign with 100 clicks
    When that campaign has served 1000 imps
    Then the CTR is 10%