Feature: Calculations of Revenue
  As the seller of this product, Adhesive would like to ensure that calculations of revenue are accurate, according to
  the specified parameters.

  Background:
    Given I am logged in as a Publisher who has multiple Sites with Revenue

  Scenario: The revenue is 30% of the Advertiser click costs when that is greater than the Pub's minCPM*(clicks/1000)
    Given the Advertiser's cost is $100
    And the Publisher's minimum CPM is $10
    When the click count is 2500
    Then the Revenue is $30

  Scenario: The revenue is the Publisher's minCPM*(clicks/1000) when that is greater than 30% of the Advertiser's click costs
    Given the Advertiser's cost is $50
    And the Publisher's minimum CPM is $10
    When the click count is 2500
    Then the Revenue is $25

  Scenario: The Publisher's Revenue is affected when the Publisher changes their minCPM value
    Given the Advertiser's cost is $75
    And the Publisher's minimum CPM is $15
    And the click count is 1000
    When the Publisher updates the min CPM to $10
    And there are an addition 500 clicks
    Then the Revenue is $22.50
