Feature: The selection criteria for serving an appropriate ad
  As an advertiser, I want to ensure that the right ad is shown to the right prospect

  Scenario: Default action taken when request is for a bad ID
    Given there is ad request for a Site ID that does not exist
    Then a default action is taken

  Scenario: Default action taken when request has bad domain
    When there is an ad request with a bad domain
    Then a default action is taken

  Scenario: Default action taken when no contextual candidates exist
    Given there is a valid ad request
    When no contextual candidates exist
    Then a default action is taken

  Scenario: Default action taken when above spending limit
    Given there is a valid ad request
    When the spending limit has been reached
    Then a default action is taken

  Scenario: Default action taken when above frequency cap
    Given there is a valid ad request
    When the prospect has already been served the limit for the ad type ("contextual candidate"?)
    Then a default action is taken

  Scenario: Default action taken when advertiser excluded from the site
    Given there is a valid ad request
    When the advertiser is excluded from serving ads on the requesting site
    Then a default action is taken

  Scenario: Ad shown when the Publisher's count is above threshold and Estimated ECPM > Publisher's minimum CPM
    Given there is a valid ad request
    And the publisher for the ad has performance numbers
    And the count of ads shown for this publisher for this contextual candidate (?) is more than 10,000 for the last 7 days
    But the prospect has not been served the limit for the ad type for the day
    And the spending limit has not been reached
    And the advertiser is not excluded from serving that ad type on the requesting site
    And the actual 7-day click-through rate for the ad type is 3%
    And the CPC Bid for that ad type is $3.00
    # This means ECPM equals 3% * 1000 * $3.00 * 0.3 = 27
    And the Publisher Site's minCPM is $20.00
    # 90 > 10, so the ad is not filtered
    And one ad matches
    Then that ad is displayed

  Scenario: Ad not shown when the Publisher's count is above threshold and Estimated ECPM < Publisher's minimum CPM
    Given there is a valid ad request
    And the publisher for the ad has performance numbers
    And one ad that is a contextual candidate exists
    And the count of ads shown for this publisher for this contextual candidate (?) is more than 10,000 for the last 7 days
    But the prospect has not been served the limit for the ad type for the day
    And the spending limit has not been reached
    And the advertiser is not excluded from serving that ad type on the requesting site
    And the actual 7-day click-through rate for the ad type is 1%
    And the CPC Bid for that ad type is $3.50
  # This means ECPM equals 1% * 1000 * $3.50 * 0.3 = 10.5
    And the Publisher Site's minCPM is $11.00
  # 10.5 < 11, so the ad is filtered
    Then a default action is taken

  Scenario: Ad shown when we don't have performance numbers for the Publisher
    Given there is a valid ad request
    And the publisher for the ad does not have any performance data yet
    And one ad that is a contextual candidate exists
    And the prospect has not been served the limit for the ad type for the day
    And the spending limit has not been reached
    And the advertiser is not excluded from serving that ad type on the requesting site
    And the CPC Bid for that ad type is $3.50
  # This means ECPM equals 1% * 1000 * $3.50 * 0.3 = 10.5
    And the Publisher Site's minCPM is $10.00
  # 10.5 > 10, so the ad is filtered
    Then that ad is displayed

  Scenario: Ad not shown when we don't have performance numbers for the Publisher
    Given there is a valid ad request
    And the publisher for the ad does not have any performance data yet
    And one ad that is a contextual candidate exists
    And the prospect has not been served the limit for the ad type for the day
    And the spending limit has not been reached
    And the advertiser is not excluded from serving that ad type on the requesting site
    And the CPC Bid for that ad type is $3.50
  # This means ECPM equals 1% * 1000 * $3.50 * 0.3 = 10.5
    And the Publisher Site's minCPM is $11.00
  # 10.5 < 11, so the ad is filtered
    Then a default action is taken

  Scenario: Ad shown when the Publisher's count is below threshold and Estimated ECPM > Publisher's minimum CPM
    Given there is a valid ad request
    And the publisher for the ad has performance numbers
    And the count of ads shown for this publisher for this contextual candidate (?) is 5,000 for the last 7 days
  # Thus .01 * 5,000 = 50 normalized clicks to add to the actual click count
    And the prospect has not been served the limit for the ad type for the day
    And the spending limit has not been reached
    And the advertiser is not excluded from serving that ad type on the requesting site
    And the actual click count for the past 7 days is 40
    And the CPC Bid for that ad type is $3.00
  # This means ECPM equals 0.9% * 1000 * 3 * .3 = 8.1
    And the Publisher Site's minCPM is $7.00
  # 8.1 > 7, so the ad is not filtered
    And one ad matches
    Then that ad is displayed

  Scenario: Ad not shown when the Publisher's count is below threshold and Estimated ECPM < Publisher's minimum CPM
    Given there is a valid ad request
    And the publisher for the ad has performance numbers
    And one ad that is a contextual candidate exists
    And the count of ads shown for this publisher for this contextual candidate (?) is 3,000 for the last 7 days
  # Thus .01 * 7,000 = 70 normalized clicks to add to actual click count
    But the prospect has not been served the limit for the ad type for the day
    And the spending limit has not been reached
    And the advertiser is not excluded from serving that ad type on the requesting site
    And the actual click count for the past 7 days is 40
    And the CPC Bid for that ad type is $3.50
  # This means ECPM equals 1.1% * 1000 * $3.50 * 0.3 = 11.55
    And the Publisher Site's minCPM is $12.00
  # 11.55 < 12, so the ad is filtered
    Then a default action is taken