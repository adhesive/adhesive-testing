Feature: Account Home, Sites tab interface for Advertisers
  As an Advertiser I want to be able to manage my site list by adding, deleting and getting
  report data for sites in my account, so I can drive ad clicks to them

  Background:
    Given I am logged in as an Advertiser who has several Sites
    And I am on the Account Home page, Sites tab

  Scenario: User should only see appropriate sites in list
    When I look at the list of Sites
    Then I see all and only the Sites that I created

  Scenario: Sites tab displays all required info
    Then the Sites table has all expected Sites and info, including the Site's status as set by an Admin

  Scenario: Advertisers can sort the Sites by columns
    Then clicking the Sites table column headers will sort and reverse sort, as expected

  Scenario: Advertisers should be able to add new Sites
    Then I should be able to create a Site

  Scenario: Advertisers should be able to delete Sites
    When I select to delete a Site
    And I confirm the Site deletion
    Then the Site status becomes "deleted" and it is removed from the Sites list

  Scenario: Advertisers cannot edit newly added Sites
    Given I create a Site
    When I edit the Site
    Then the Site info cannot be updated

  Scenario: Advertisers can see report data for their Sites
    Then I should see Click counts, Impressions, CTR, Avg. CPC, and Total Cost for my Sites

  Scenario: Advertisers can select a preset date range for their Site report info
    When I select from the list of preset date ranges
    Then the reported data updates

  Scenario: The default date range shown is "Current Month"
    Then the default date range shown is "Current Month"

  Scenario: Advertisers can select a custom date range for their Site report info
    When I select a custom date range to view
    Then the reported data updates