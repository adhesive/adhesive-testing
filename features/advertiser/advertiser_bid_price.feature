Feature: Advertisers managing account's bid price settings
  As a Advertiser, I want to be able to manage the bid price settings for my account, so I can control how much I pay

  Background:
    Given I am logged in as an Advertiser who has several Sites
    And I am on the Account Home Bidding Tab

  Scenario: The bidding tab ui contains the expected elements for Advertisers
    Then I see the bidding table with the expected columns of info
    And the campaigns displayed are the Active campaigns

  Scenario: Advertisers can edit the CPC Bid price
    Then I can edit the CPC Bid value of a Campaign

  Scenario: The CPC Bid field only accepts numbers
    When I enter invalid characters in the CPC Bid field
    Then I am told to enter a valid value
    And the entry is not saved

  Scenario: CPC Bid values entered on this screen are reflected on the Campaign Edit screen
    When I update a Campaign's CPC Bid
    And go to that Campaign's Edit page
    Then I will see the CPC Bid I entered

  Scenario: The Bidding tab only shows the active campaigns (unless no campaigns are active)
    When I change the Site's Active Campaign
    Then The Campaign displayed on this page changes to the newly active campaign