Feature: My Account settings page for the Advertiser user
  As an advertiser, I want to be able to manage my account settings, so adhesive has the proper contact info and so I have the proper billing and password information on file.

  Background:
    Given I am logged in as an Advertiser user
    And I am on the My Account page

  Scenario: The User interface displays the expected elements
    When I look at the interface
    Then It contains elements appropriate to an Advertiser user

  Scenario: User can update their account information
    When I update my information
    Then I can save the changes

  Scenario: User can update their password
    When I update my password
    Then I can log in with the new password
    And the old password no longer works to log in