Feature: Account Home page, Campaign create, read, edit, delete for Advertisers
  As an advertiser, I want to be able to manage my campaign list by adding, deleting, and getting
  report data for campaigns in my account, so I can create ads to show to users.

  Background:
    Given I am logged in as an Advertiser who has several Sites
    And I am on the Account Home page, Campaigns tab

  Scenario: Interface has expected elements
    Then I can see the expected Campaigns and info

  Scenario: Advertisers can create Campaigns
    Then I should be able to create a Campaign

  Scenario: Advertisers can edit Campaigns
    When I select a Campaign to edit
    Then I am taken to the Campaign Edit page

  Scenario: Advertisers can play/pause a campaign directly via the table
    Then the Campaigns table shows play/pause icons for the campaigns
    And the icons can be clicked to toggle the Campaign's Active status

  Scenario: Advertisers can see the Campaign status set by the Admin
    Then the listed Campaigns will display a pending/hold/enabled status

  Scenario: Campaigns created by Advertisers get a pending status
    When I create a Campaign
    Then the Campaign gets a status of Pending
    And I can't change the Campaign's status

  Scenario: Campaigns edited by Advertisers are paused
    When I edit a Campaign
    Then the Campaign is paused

  Scenario: Advertisers can update Campaign info
    When I select a Campaign to edit
    Then I can update Campaign information

  Scenario: Campaign file assets should be selectable
    When I select a Campaign to edit
    Then the uploaded file assets should be listed in the selection boxes

  Scenario: Advertisers can preview ads
    When I preview an ad for a Campaign
    Then an example of the ad should appear in a pop-up window

  Scenario: Advertisers can view summary report data for campaigns
    Then I should see data for Clicks, Impressions, CTR, Avg. CPC, and Total cost
    And the default date range should be "Current month"

  Scenario: Advertisers can update the selected date range for reported data
    Then I should be able to select from a preset list of date ranges
    And I should be able to select a custom date range