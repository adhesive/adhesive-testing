Feature: Advertiser's Site reporting summary data
  As an advertiser, I want to be able to view report data for the sites in my site list, so I can determine my ROI

  Background:
    Given I am logged in as an Advertiser who has several Sites
    And I am on the Account Home page, Campaigns tab

  Scenario: Click counts are accurate for the selected date range

  Scenario: Impression counts are accurate for the selected date range

  Scenario: Click-through rates are accurate for the selected date range

  Scenario: Average Cost Per Clicks are accurate for the selected date range

  Scenario: Cost is accurate for the selected date range