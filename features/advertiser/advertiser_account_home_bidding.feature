Feature: Advertiser's Account Home, Bidding tab reporting data
  As an advertiser, I want the report data generated so later I can see it in the bidding tab

  Background:
    Given I am logged in as an Advertiser who has several Sites
    And I am on the Account Home Bidding Tab

  Scenario: Click counts are accurate for the selected date range

  Scenario: Impression counts are accurate for the selected date range

  Scenario: Click-through rates are accurate for the selected date range

  Scenario: Average Cost Per Clicks are accurate for the selected date range

  Scenario: Cost is accurate for the selected date range

  Scenario: