Feature: Multiple Tag Types
  As an advertiser, I want to configure a bid price for each tag type, so that I can bid more appropriately based upon where the user is in the funnel.

  Scenario: Each tag type can have a bid price