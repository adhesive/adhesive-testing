Feature: User Account Password

  As an Adhesive user, I want to have a secret, custom password to use for logging in to the site to manage
  my sites/settings.

  Background:
    Given I am updating My Account Profile

  Scenario: User's password can be updated
    Given I enter my correct current password
    When I enter a valid new password
    And I enter the same confirmation password
    Then I can log in with the new password

  Scenario: User's password can't be too short
    Given I enter my correct current password
    When I enter a new password and confirmation that is too short
    Then I am told it is too short
    And I can't update my password

  Scenario: New password and confirmation password must match
    Given I enter my correct current password
    When I enter a valid new password
    But I enter a different confirmation password
    Then I am told that the passwords must match
    And I can't update my password

  Scenario: User must enter correct current password to update
    When I enter an incorrect current password
    Then I am told that I must enter the correct password
    And I can't update my password
