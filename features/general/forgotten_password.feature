Feature: Forgotten password retrieval
  As an Adhesive user, I want to be able to reset my account password, so if I forgot it I can still log in

  Scenario: Retrieval instructions are emailed to valid user email addresses
    Given I am on the Forgot My Password page
    When I submit my username/email
    Then I will receive an email with a link to reset my password

  Scenario: Invalid username entered should result in?

  Scenario: Links to reset used after 24-hour period should tell users to re-submit via the "Forgot" form

  Scenario: Links to reset used more than once should tell users to re-submit via the "Forgot" form