Feature: User Account Profile

  As an Adhesive user I want to be able to update my profile information as necessary, and be required to enter
  all information necessary to ensure I receive payments.

  Background:
    Given I am updating My Account Profile

  Scenario: Users can update their profile information
    When I change the information in the Account Profile
    Then the new information is saved

  Scenario: Updated email address works to log in
    When I change my email address
    Then I can log in with the new email
    And the old email address no longer works to log in

  Scenario: Field requirements are properly enforced
    When I try to update the profile info when a required field is blank
    Then I am told that the field is required
    And the update is not allowed