Feature: Registration of a new Advertiser Account
  As an advertiser, I want to be able to register so I can buy clickthroughs via ads driven by Adhesive

  Background:
    Given I am on the Registration page

  Scenario: Field requirements are enforced
    # TODO

  Scenario: Registration creates the Advertiser Account
    When I have registered
    Then I should be taken directly to the Account Home page