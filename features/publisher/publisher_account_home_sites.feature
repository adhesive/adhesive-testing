Feature: Publisher Home, Sites Date Ranges
  As a publisher, I want to be able to view report data for the Sites in my Site list, so I can determine my ROI.

  Background:
    Given I am logged in as a Publisher with several sites with data
    And I am on the Publisher Home page, Sites tab

  Scenario: User can select from a list of preset date ranges for displaying Site data
    When I select a date range from the preset list
    Then The data displayed on the page updates to show data appropriate to the selected range

  Scenario: User can select a custom date range for displaying Site data
    When I specify a custom date range to display
    Then The data displayed on the page updates to show data appropriate to the selected range