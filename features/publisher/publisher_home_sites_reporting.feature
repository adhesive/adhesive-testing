Feature: Publisher Home, Sites Reporting Data
  As a publisher, I want to be able to view report data for the Sites in my Site list, so I can determine my ROI.

  Background:
    Given I am logged in as a Publisher with several sites with data
    And I am on the Publisher Home page, Sites tab

  Scenario: Impression counts are accurate for selected Sites and date ranges
    # Given/When/Then TBD

  Scenario: % of Impressions Taken is accurate for the selected Sites and date ranges
    # Given/When/Then TBD

  Scenario: Click counts are accurate for selected Sites and date ranges
    # Given/When/Then TBD

  Scenario: The ECPM value is accurate for selected Sites and date ranges
    # Given/When/Then TBD

  Scenario: The Revenue value is accurate for the selected Sites and date ranges
